#!/bin/bash

# shellcheck source=scripts/script-parameters.sh
. "$(dirname "${BASH_SOURCE[0]}")"/script-parameters.sh local
. "$(dirname "${BASH_SOURCE[0]}")"/selection-site.sh "$@"

for DRUPAL_SITE in "${DRUPAL_SITES_LIST[@]}"
do
  CURRENT_SITE_FOLDER_NAME="DRUPAL_SITE_${DRUPAL_SITE^^}_FOLDER_NAME"
  CURRENT_SITE_BACKUP_FILES="DRUPAL_SITE_${DRUPAL_SITE^^}_BACKUP_FILES"

  . "${SCRIPTS_PATH}"/tasks/dump_database.sh

  if [ "${!CURRENT_SITE_BACKUP_FILES}" = "true" ]; then
    echo -e "${COLOR_LIGHT_GREEN}Public files backup.${NC}"
    cp -R "${APP_PATH}/sites/${!CURRENT_SITE_FOLDER_NAME}/files" "${PROJECT_PATH}/backups/${!CURRENT_SITE_FOLDER_NAME}/${CURRENT_DATE}/files-${!CURRENT_SITE_FOLDER_NAME}"

    echo -e "${COLOR_LIGHT_GREEN}Private files backup.${NC}"
    cp -R "${PROJECT_PATH}/private_files/${!CURRENT_SITE_FOLDER_NAME}" "${PROJECT_PATH}/backups/${CURRENT_DATE}/${!CURRENT_SITE_FOLDER_NAME}/private_files-${!CURRENT_SITE_FOLDER_NAME}"
  fi
done
