<?php

declare(strict_types = 1);

use drupol\PhpCsFixerConfigsDrupal\Config\Drupal8;

$custom_config = new Drupal8();

$custom_config->setCacheFile('/tmp/.php_cs.cache');

// Overrides rules provided by vendor.
$rules = $custom_config->getRules();

$rules['declare_strict_types'] = TRUE;

$rules['doctrine_annotation_spaces']['before_argument_assignments'] = TRUE;
$rules['doctrine_annotation_spaces']['after_argument_assignments'] = TRUE;

$custom_config->setRules($rules);

// @todo Files without .php extension are not detected.

return $custom_config;
