#!/bin/bash

CURRENT_SITE_DRUSH_ALIAS="DRUPAL_SITE_${DRUPAL_SITE^^}_DRUSH_ALIAS"

echo -e "${COLOR_LIGHT_GREEN}${DRUPAL_SITE}: Launch post updates.${COLOR_NC}"
$DRUSH "${!CURRENT_SITE_DRUSH_ALIAS}" updatedb -y

echo -e "${COLOR_LIGHT_GREEN}${DRUPAL_SITE}: Launch post updates a second time. Just in case...${COLOR_NC}"
$DRUSH "${!CURRENT_SITE_DRUSH_ALIAS}" updatedb -y
