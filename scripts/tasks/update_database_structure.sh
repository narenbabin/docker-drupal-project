#!/bin/bash

CURRENT_SITE_DRUSH_ALIAS="DRUPAL_SITE_${DRUPAL_SITE^^}_DRUSH_ALIAS"

echo -e "${COLOR_LIGHT_GREEN}${DRUPAL_SITE}: Launch updates (no post updates). Ensure that the database schema is up-to-date.${COLOR_NC}"
$DRUSH "${!CURRENT_SITE_DRUSH_ALIAS}" updatedb --no-post-updates -y

echo -e "${COLOR_LIGHT_GREEN}${DRUPAL_SITE}: Launch updates (no post updates) a second time. Just in case...${COLOR_NC}"
$DRUSH "${!CURRENT_SITE_DRUSH_ALIAS}" updatedb --no-post-updates -y
