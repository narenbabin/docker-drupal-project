# Docker Drupal project

[![pipeline status](https://gitlab.com/florenttorregrosa-drupal/docker-drupal-project/badges/9.x/pipeline.svg)](https://gitlab.com/florenttorregrosa-drupal/docker-drupal-project/-/commits/9.x)

A Drupal project template with Docker environment.

## Branches

* [9.x](https://gitlab.com/florenttorregrosa-drupal/docker-drupal-project/-/tree/9.x): Default branch.
* [ci-tests](https://gitlab.com/florenttorregrosa-drupal/docker-drupal-project/-/tree/ci-tests): Branch to have failing tests and ensure they detect errors.
* [ci-tests-example](https://gitlab.com/florenttorregrosa-drupal/docker-drupal-project/-/tree/ci-tests-example): Branch to have some examples of custom config/code and tests written against it.
* [ci-contrib](https://gitlab.com/florenttorregrosa-drupal/docker-drupal-project/-/tree/ci-contrib): To monitor projects I maintain/co-maintain on Drupal.org and be able to apply additional quality tools not present on Drupal.org.
* [ci-contrib-smile](https://gitlab.com/florenttorregrosa-drupal/docker-drupal-project/-/tree/ci-contrib-smile): To monitor projects I maintain/co-maintain on Drupal.org sponsored by [Smile](https://www.smile.eu) and be able to apply additional quality tools not present on Drupal.org.
* [setup-entity-share](https://gitlab.com/florenttorregrosa-drupal/docker-drupal-project/-/tree/setup-entity-share): Adaptation into a multi-site for [Entity Share](https://www.drupal.org/project/entity_share) development/testing/demonstration.

All branches are rebased against the `9.x` branch.

## Documentation

See the [docs](docs) folder for detailed documentation:

* [Requirements](docs/00-requirements.md)
* [Installation](docs/01-installation.md)
* [Docker](docs/02-docker.md)
* [Tools](docs/03-tools.md)
* [Development tips](docs/04-development.md)
* [Troubleshooting](docs/05-troubleshooting.md)
* [Core update](docs/06-core-update.md)
* [Gitlab CI tips](docs/07-gitlab-ci.md)
* [PHPStorm configuration](docs/08-phpstorm.md)
* [Deployment structure and scripts](docs/09-deployment.md)
* [Secrets handling](docs/10-secrets.md)
* [Logs handling](docs/11-logs.md)
